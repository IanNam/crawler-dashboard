// in src/posts.js
import React from 'react';
import { List, Datagrid, TextField } from 'admin-on-rest';

export const PostList = (props) => (
    <List {...props}>
        <Datagrid>
            <TextField source="id" />
            <TextField source="url" />
            <TextField source="status" />
            <TextField source="title" />
            <TextField source="fetchTime" />
            <TextField source="fetchInterval" />
            <TextField source="prevFetchTime" />
        </Datagrid>
    </List>
);