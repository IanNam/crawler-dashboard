// in src/App.js
import React from 'react';

import { jsonServerRestClient, Admin, Resource } from 'admin-on-rest';

import { PostList } from './posts';

const App = () => (
    <Admin restClient={jsonServerRestClient('http://localhost:5050')}>
        <Resource name="posts" list={PostList} />
    </Admin>
);

export default App;